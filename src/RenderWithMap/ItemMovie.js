import React, { Component } from "react";
export default class ItemMovie extends Component {
  render() {
    let movie = this.props.data;
    let {
      maPhim,
      tenPhim,
      biDanh,
      trailer,
      hinhAnh,
      moTa,
      maNhom,
      ngayKhoiChieu,
      danhGia,
    } = movie;
    return (
      <div className="col-4">
        <div className="card text-left">
          <img className="card-img-top" src={hinhAnh} alt="" />
          <div className="card-body">
            <p>{maPhim}</p>
            <p>{tenPhim}</p>
            <p>{biDanh}</p>
            <p>{trailer}</p>
            <p>{moTa}</p>
            <p>{maNhom}</p>
            <p>{ngayKhoiChieu}</p>
            <p>{danhGia}</p>
          </div>
        </div>
      </div>
    );
  }
}
