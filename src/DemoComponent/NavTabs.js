import React, { Component } from "react";

export default class NavTabs extends Component {
  render() {
    return (
      <div>
        <nav>
          <div className="nav nav-tabs text-center" id="nav-tab" role="tablist">
            <a
              className="nav-item nav-link active"
              id="nav-home-tab"
              data-toggle="tab"
              href="#nav-home"
              role="tab"
              aria-controls="nav-home"
              aria-selected="true"
            >
              Home
            </a>
            <a
              className="nav-item nav-link"
              id="nav-profile-tab"
              data-toggle="tab"
              href="#nav-profile"
              role="tab"
              aria-controls="nav-profile"
              aria-selected="false"
            >
              Profile
            </a>
            <a
              className="nav-item nav-link"
              id="nav-contact-tab"
              data-toggle="tab"
              href="#nav-contact"
              role="tab"
              aria-controls="nav-contact"
              aria-selected="false"
            >
              Contact
            </a>
          </div>
        </nav>
        <div className="tab-content" id="nav-tabContent">
          <div
            className="tab-pane fade show active"
            id="nav-home"
            role="tabpanel"
            aria-labelledby="nav-home-tab"
          >
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis
            obcaecati nemo eos unde amet architecto accusamus similique eum
            blanditiis autem. Porro, minus veritatis consequatur praesentium ut
            corrupti et doloribus. Obcaecati.
          </div>
          <div
            className="tab-pane fade"
            id="nav-profile"
            role="tabpanel"
            aria-labelledby="nav-profile-tab"
          >
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis
            obcaecati nemo eos unde amet architecto accusamus similique eum
            blanditiis autem. Porro, minus veritatis consequatur praesentium ut
            corrupti et doloribus. Obcaecati.
          </div>
          <div
            className="tab-pane fade"
            id="nav-contact"
            role="tabpanel"
            aria-labelledby="nav-contact-tab"
          >
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis
            obcaecati nemo eos unde amet architecto accusamus similique eum
            blanditiis autem. Porro, minus veritatis consequatur praesentium ut
            corrupti et doloribus. Obcaecati.
          </div>
        </div>
      </div>
    );
  }
}
