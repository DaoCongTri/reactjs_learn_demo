import React from "react";

export default function Carts() {
  return (
    <div className="container">
      <div className="row">
        <div className="col-4 mb-lg-3 mb-lg-3">
          <div className="card">
            <img
              src="https://media-cdn-v2.laodong.vn/Storage/NewsPortal/2021/12/17/985676/Rose-Blackpink.jpeg"
              className="card-img-top"
              alt="..."
            />
            <div className="card-body">
              <h5 className="card-title">Card title</h5>
              <p className="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </p>
              <a href="#" className="btn btn-primary">
                Go somewhere
              </a>
            </div>
          </div>
        </div>
        <div className="col-4 mb-lg-3">
          <div className="card">
            <img
              src="https://media-cdn-v2.laodong.vn/Storage/NewsPortal/2021/12/17/985676/Rose-Blackpink.jpeg"
              className="card-img-top"
              alt="..."
            />
            <div className="card-body">
              <h5 className="card-title">Card title</h5>
              <p className="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </p>
              <a href="#" className="btn btn-primary">
                Go somewhere
              </a>
            </div>
          </div>
        </div>
        <div className="col-4 mb-lg-3">
          <div className="card">
            <img
              src="https://media-cdn-v2.laodong.vn/Storage/NewsPortal/2021/12/17/985676/Rose-Blackpink.jpeg"
              className="card-img-top"
              alt="..."
            />
            <div className="card-body">
              <h5 className="card-title">Card title</h5>
              <p className="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </p>
              <a href="#" className="btn btn-primary">
                Go somewhere
              </a>
            </div>
          </div>
        </div>
        <div className="col-4 mb-lg-3">
          <div className="card">
            <img
              src="https://media-cdn-v2.laodong.vn/Storage/NewsPortal/2021/12/17/985676/Rose-Blackpink.jpeg"
              className="card-img-top"
              alt="..."
            />
            <div className="card-body">
              <h5 className="card-title">Card title</h5>
              <p className="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </p>
              <a href="#" className="btn btn-primary">
                Go somewhere
              </a>
            </div>
          </div>
        </div>
        <div className="col-4 mb-lg-3">
          <div className="card">
            <img
              src="https://media-cdn-v2.laodong.vn/Storage/NewsPortal/2021/12/17/985676/Rose-Blackpink.jpeg"
              className="card-img-top"
              alt="..."
            />
            <div className="card-body">
              <h5 className="card-title">Card title</h5>
              <p className="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </p>
              <a href="#" className="btn btn-primary">
                Go somewhere
              </a>
            </div>
          </div>
        </div>
        <div className="col-4 mb-lg-3">
          <div className="card">
            <img
              src="https://media-cdn-v2.laodong.vn/Storage/NewsPortal/2021/12/17/985676/Rose-Blackpink.jpeg"
              className="card-img-top"
              alt="..."
            />
            <div className="card-body">
              <h5 className="card-title">Card title</h5>
              <p className="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </p>
              <a href="#" className="btn btn-primary">
                Go somewhere
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
