import React, { Component } from "react";

export default class DemoState extends Component {
  // state ~ setState
  // ko update trực tiếp giá trị state, phải update thông qua setState
  state = {
    soLuong: 1,
  };
  handleUp = () => {
    console.log("Up");
    this.setState({
      // ko thể ghi ++
      soLuong: this.state.soLuong + 1,
    });
  };
  handleDown = () => {
    console.log("Down");
    this.setState({
      soLuong: this.state.soLuong - 1,
    });
  };
  render() {
    return (
      <div>
        <h1 style={{ color: "red", background: "blue" }}>Demo State</h1>
        <button onClick={this.handleDown}>-</button>
        <span>{this.state.soLuong}</span>
        <button onClick={this.handleUp}>+</button>
      </div>
    );
  }
}
