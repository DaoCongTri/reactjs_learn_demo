import React, { Component } from "react";

export default class ExStateCar extends Component {
  state = {
    url: "./img/CarBasic/products/red-car.jpg",
  };
  handleChangeColor = (color) => {
    this.setState({
      url: `./img/CarBasic/products/${color}-car.jpg`,
    });
  };
  render() {
    return (
      <div className="row">
        {/* Luôn dẫn từ index.html */}
        <img className="col-4" src={this.state.url} alt="" />
        <button
          onClick={() => {
            this.handleChangeColor("red");
          }}
          className="btn btn-danger"
        >
          Red
        </button>
        <button
          onClick={() => {
            this.handleChangeColor("black");
          }}
          className="btn btn-dark mx-5"
        >
          Black
        </button>
        <button
          onClick={() => {
            this.handleChangeColor("steel");
          }}
          className="btn btn-info"
        >
          Steel
        </button>
        <button
          onClick={() => {
            this.handleChangeColor("silver");
          }}
          className="btn btn-secondary mx-5"
        >
          Silver
        </button>
      </div>
    );
  }
}
