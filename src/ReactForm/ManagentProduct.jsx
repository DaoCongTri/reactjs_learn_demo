import React, { Component } from "react";
import ProductList from "./ProductList";
export default class ManagentProduct extends Component {
  state = {
    values: {
      id: "",
      img: "",
      name: "",
      price: 0,
      type: "",
      desc: "",
    },
    errors: {
      id: "",
      img: "",
      name: "",
      price: "",
      desc: "",
    },
    listProduct: [
      {
        id: "1",
        img: "https://cdn2.cellphones.com.vn/x358,webp,q100/media/catalog/product/s/m/sm-s908_galaxys22ultra_front_burgundy_211119_2.jpg",
        name: "Samsung Galaxy M53 + Plus",
        price: 1000,
        type: "Samsung",
        desc: "ABC",
      },
    ],
  };
  formInValid = () => {
    // chưa đúng format
    let inValid = false;
    Object.values(this.state.values).forEach((value) => {
      if (!value) {
        inValid = true;
      }
      return inValid;
    });
  };
  handleOnChange = (event) => {
    const { id, value } = event.target;
    const newValue = { ...this.state.values, [id]: value };
    const newError = { ...this.state.errors };
    if (!value.trim()) {
      newError[id] = "Trường này không được bỏ trống";
    } else {
      const regex = /^[0-9]{1,4}$/;
      const inValid = regex.test(value);
      if (!inValid) {
        newError[id] = "Trường nhập không đúng định dạng";
      } else {
        newError[id] = "";
      }
    }
    this.setState({ values: newValue, errors: newError });
  };
  handleSubmit = (event) => {
    event.preventDefault();
    // copy ra 1 list mới và thêm vào 1 sản phẩm: this.state.values
    if (this.formInValid()) {
      const newProduct = [...this.state.listProduct, this.state.values];
      this.setState({ listProduct: newProduct });
    }
  };
  handleOnBlur = (event) => {
    const { id, value } = event.target;
    const newValue = { ...this.state.values, [id]: value };
    const newError = { ...this.state.errors };
    if (!value.trim()) {
      newError[id] = "Trường này không được bỏ trống";
    } else {
      // xử lý ko đúng định dạng
      newError[id] = "";
    }
    this.setState({ values: newValue, errors: newError });
  };
  render() {
    let { values, errors, listProduct } = this.state;
    let { id, img, name, price, type, desc } = values;
    return (
      <div className="container">
        <h1 className="text-center p-5">Create Product</h1>
        <div style={{ border: 1, borderColor: "black", borderStyle: "solid" }}>
          <h1 className="bg-dark p-4 text-warning">Product Info</h1>
          <form action="" onSubmit={this.handleSubmit}>
            <div className="row p-4">
              <div className="col-6">
                <label htmlFor="">ID:</label>
                <br />
                <input
                  type="text"
                  id="id"
                  value={id}
                  className="form-control"
                  onChange={this.handleOnChange}
                  onBlur={this.handleOnBlur}
                />
                {errors.id && <span className="text-danger">{errors.id}</span>}
              </div>
              <div className="col-6">
                <label htmlFor="">Image:</label>
                <br />
                <input
                  type="text"
                  id="img"
                  value={img}
                  className="form-control"
                  onChange={this.handleOnChange}
                  onBlur={this.handleOnBlur}
                />
                {errors.img && (
                  <span className="text-danger">{errors.img}</span>
                )}
              </div>
              <div className="col-6">
                <label htmlFor="">Name:</label>
                <br />
                <input
                  type="text"
                  id="name"
                  value={name}
                  className="form-control"
                  onChange={this.handleOnChange}
                  onBlur={this.handleOnBlur}
                />
                {errors.name && (
                  <span className="text-danger">{errors.name}</span>
                )}
              </div>
              <div className="col-6">
                <label htmlFor="">Product Type:</label>
                <br />
                <select
                  name=""
                  id="type"
                  value={type}
                  className="form-control"
                  onChange={this.handleOnChange}
                  onBlur={this.handleOnBlur}
                >
                  <option value="0">Choose Type</option>
                  <option value="iPhone">iPhone</option>
                  <option value="Samsung">Samsung</option>
                </select>
                {errors.type && (
                  <span className="text-danger">{errors.type}</span>
                )}
              </div>
              <div className="col-6">
                <label htmlFor="">Price:</label>
                <br />
                <input
                  type="text"
                  id="price"
                  value={price}
                  className="form-control"
                  onChange={this.handleOnChange}
                  onBlur={this.handleOnBlur}
                />
                {errors.price && (
                  <span className="text-danger">{errors.price}</span>
                )}
              </div>
              <div className="col-6">
                <label htmlFor="">Description:</label>
                <br />
                <textarea
                  name=""
                  id="desc"
                  cols="30"
                  rows="5"
                  value={desc}
                  className="form-control"
                  onChange={this.handleOnChange}
                  onBlur={this.handleOnBlur}
                ></textarea>
                {errors.desc && (
                  <span className="text-danger">{errors.desc}</span>
                )}
              </div>
            </div>
            <div className="p-4 bg-secondary">
              <button className="btn btn-primary mr-4">Create</button>
              <button className="btn btn-success">Update</button>
            </div>
          </form>
        </div>
        <ProductList data={this.state.listProduct} />
      </div>
    );
  }
}
