import React, { Component } from "react";

export default class extends Component {
  render() {
    let { data } = this.props;
    return (
      <div className="mt-5">
        <table className="table">
          <thead className="bg-secondary text-warning">
            <tr>
              <th>ID</th>
              <th>Image</th>
              <th>Name</th>
              <th>Price</th>
              <th>Product Type</th>
              <th>Description</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {data.map((item) => {
                let {id, img, name, price, type, desc} = item;
              return (
                <tr key={id}>
                  <td scope="row">{id}</td>
                  <td>
                    <img
                      src={img}
                      alt=""
                      width={150}
                      height={150}
                      style={{ objectFit: "cover" }}
                    />
                  </td>
                  <td>{name}</td>
                  <td>{price}</td>
                  <td>{type}</td>
                  <td>{desc}</td>
                  <td>
                    <button className="btn btn-secondary mr-3">Update</button>
                    <button className="btn btn-danger">Delete</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
