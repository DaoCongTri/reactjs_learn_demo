import React, { Component } from "react";

export default class Cart extends Component {
  render() {
    let { cart, handleDelete, handleChangeAmount } = this.props;
    return (
      <div className="col-6">
        <h1 className="text-center bg-dark text-white p-4">Cart</h1>
        <table class="table">
          <thead>
            <tr>
              <th>Image</th>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Total</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {cart.map((item, index) => {
              let { id, name, price, image, soLuong } = item;
              return (
                <tr key={index}>
                  <td>
                    <img src={image} alt="" width={100} height={100} />
                  </td>
                  <td>{name}</td>
                  <td>{price}</td>
                  <td>
                    <button
                      className=""
                      onClick={() => {
                        handleChangeAmount(id, "Down");
                      }}
                    >
                      -
                    </button>
                    {soLuong}
                    <button
                      className=""
                      onClick={() => {
                        handleChangeAmount(id, "Up");
                      }}
                    >
                      +
                    </button>
                  </td>
                  <td>{price * soLuong}</td>
                  <td>
                    <button
                      className="btn btn-danger"
                      onClick={() => {
                        handleDelete(id);
                      }}
                    >
                      X
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
