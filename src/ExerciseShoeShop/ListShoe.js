import React, { Component } from "react";
import ItemShoe from "./ItemShoe";
export default class ListShoe extends Component {
  renderListShoe() {
    return this.props.list.map((shoe, index) => {
      return (
        <ItemShoe
          handleViewDetail={this.props.handleViewDetail}
          key={index}
          data={shoe}
          handleBuyShoe={this.props.handleBuy}
        />
      );
    });
  }
  render() {
    return (
      <div className="col-6">
        <h1 className="text-white bg-dark p-4 text-center">Shoes Product</h1>
        <div className="row">{this.renderListShoe()}</div>
      </div>
    );
  }
}
