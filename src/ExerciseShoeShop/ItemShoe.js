import React, { Component } from "react";
export default class ItemShoe extends Component {
  render() {
    let { data, handleViewDetail, handleBuyShoe } = this.props;
    let { name, price, image } = data;
    return (
      <div className="col-4 pt-5">
        <div className="card text-left">
          <img
            className="card-img-top"
            src={image}
            alt=""
            width={200}
            height={200}
          />
          <div className="card-body">
            <p>{name}</p>
            <p>{price} $</p>
          </div>
          <button
            onClick={() => {
              handleViewDetail(data);
            }}
            className="btn btn-secondary"
          >
            Xem chi tiết
          </button>
          <button
            onClick={() => {
              handleBuyShoe(data);
            }}
            className="btn btn-success"
          >
            Thêm vào giỏ hàng
          </button>
        </div>
      </div>
    );
  }
}
