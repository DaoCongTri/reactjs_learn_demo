import React, { Component } from "react";
// import "./Style.css";
export default class DetailShoe extends Component {
  render() {
    let { image, name, price, description, quantity } = this.props.detail;
    return (
      <div>
        <h2>Detail</h2>
        <table class="table">
          <thead>
            <tr>
              <th>Image</th>
              <th>Name</th>
              <th>Price</th>
              <th>Description</th>
              <th>Stock</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td scope="row">
                <img src={image} alt="" width={150} height={150} />
              </td>
              <td>{name}</td>
              <td>{price}</td>
              <td>{description}</td>
              <td>{quantity}</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
