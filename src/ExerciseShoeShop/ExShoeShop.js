import React, { Component } from "react";
import { shoeArr } from "./data";
import ListShoe from "./ListShoe";
import DetailShoe from "./DetailShoe";
import Cart from "./Cart";
export default class ExShoeShop extends Component {
  state = {
    shoeArr: shoeArr,
    detailShoe: shoeArr[0],
    cart: [],
  };
  handleViewDetail = (shoe) => {
    this.setState({ detailShoe: shoe });
  };
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    // copy shoe vào newShoe và thêm key quantity
    let index = cloneCart.findIndex((item) => item.id === shoe.id);
    if (index === -1) {
      // chưa có
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    } else {
      // có => tăng soLuong lên 1 đơn vị
      cloneCart[index].soLuong++;
    }
    // th1: sp chưa có trong giỏ hàng => push
    // th2: sp đã có trong giỏ hàng => ko push
    this.setState({ cart: cloneCart });
  };
  handleDelete = (idShoe) => {
    let cloneCart = this.state.cart.filter((item) => item.id !== idShoe);
    this.setState({ cart: cloneCart });
  };
  handleChangeAmount = (idShoe, option) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => item.id === idShoe);
    console.log(cloneCart);
    if (option === "Up") {
      cloneCart[index].soLuong++;
    } else {
      cloneCart[index].soLuong--;
    }
    // cloneCart[index].soLuong += option;
    // soLuong có thể +1 hoặc -1
    if (cloneCart[index].soLuong === 0) {
      // sau khi update soLuong, nếu = 0 thì xóa
      cloneCart.splice(index, 1);
    }
    this.setState({ cart: cloneCart });
  };
  render() {
    return (
      <div className="container">
        <div className="row">
          <ListShoe
            handleBuy={this.handleAddToCart}
            handleViewDetail={this.handleViewDetail}
            list={this.state.shoeArr}
          />
          <Cart
            cart={this.state.cart}
            handleDelete={this.handleDelete}
            handleChangeAmount={this.handleChangeAmount}
          />
        </div>
        <DetailShoe detail={this.state.detailShoe} />
      </div>
    );
  }
}
