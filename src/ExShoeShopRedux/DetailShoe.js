import React, { Component } from "react";
import { connect } from "react-redux";
class DetailShoe extends Component {
  render() {
    let { image, name, price, description, quantity } = this.props.detail;
    return (
      <div>
        <h2>Detail</h2>
        <table class="table">
          <thead>
            <tr>
              <th>Image</th>
              <th>Name</th>
              <th>Price</th>
              <th>Description</th>
              <th>Stock</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td scope="row">
                <img src={image} alt="" width={150} height={150} />
              </td>
              <td>{name}</td>
              <td>{price}</td>
              <td>{description}</td>
              <td>{quantity}</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    detail: state.shoeReducer.detailShoe,
  };
};
export default connect(mapStateToProps, null)(DetailShoe);
