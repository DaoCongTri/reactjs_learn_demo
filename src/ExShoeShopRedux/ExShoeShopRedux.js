import React, { Component } from "react";
import ListShoe from "./ListShoe";
import DetailShoe from "./DetailShoe";
import Cart from "./Cart";
export default class ExShoeShopRedux extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <ListShoe />
          <Cart />
        </div>
        <DetailShoe />
      </div>
    );
  }
}
