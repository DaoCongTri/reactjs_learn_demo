import React, { Component } from "react";
import { connect } from "react-redux";
import { viewAddToCart, viewDetailAction } from "./redux/action/shoeAction";
class ItemShoe extends Component {
  render() {
    let { data } = this.props;
    let { name, price, image } = data;
    return (
      <div className="col-4 pt-5">
        <div className="card text-left h-100">
          <img
            className="card-img-top"
            src={image}
            alt=""
            width={200}
            height={200}
          />
          <div className="card-body">
            <p>{name}</p>
            <p>{price} $</p>
          </div>
          <button
            onClick={() => {
              this.props.handleChangeDetail(data);
            }}
            className="btn btn-secondary"
          >
            Xem chi tiết
          </button>
          <button
            onClick={() => {
              this.props.handleAddToCart(data);
            }}
            className="btn btn-success"
          >
            Thêm vào giỏ hàng
          </button>
        </div>
      </div>
    );
  }
}
let mapDispatchToState = (dispatch) => {
  return {
    handleChangeDetail: (data) => {
      dispatch(viewDetailAction(data));
      // before: dispatch action
      // now: dispatch fuction, function return action
    },
    handleAddToCart: (data) => {
      dispatch(viewAddToCart(data));
    },
  };
};
export default connect(null, mapDispatchToState)(ItemShoe);
