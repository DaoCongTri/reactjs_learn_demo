import React, { Component } from "react";
import { connect } from "react-redux";
import { viewChangeAmount, viewDelete } from "./redux/action/shoeAction";

class Cart extends Component {
  render() {
    let { cart } = this.props;
    return (
      <div className="col-6">
        <h1 className="text-center bg-dark text-white p-4">Cart</h1>
        <table class="table">
          <thead>
            <tr>
              <th>Image</th>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Total</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {cart.map((item, index) => {
              let { id, name, price, image, quantity } = item;
              return (
                <tr key={index}>
                  <td>
                    <img src={image} alt="" width={100} height={100} />
                  </td>
                  <td>{name}</td>
                  <td>{price}</td>
                  <td>
                    <button
                      className=""
                      onClick={() => {
                        this.props.handleChangeAmount(id, "Down");
                      }}
                    >
                      -
                    </button>
                    {quantity}
                    <button
                      className=""
                      onClick={() => {
                        this.props.handleChangeAmount(id, "UP");
                      }}
                    >
                      +
                    </button>
                  </td>
                  <td>{price * quantity}</td>
                  <td>
                    <button
                      className="btn btn-danger"
                      onClick={() => {
                        this.props.handleDelete(id);
                      }}
                    >
                      X
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    cart: state.shoeReducer.listCart,
  };
};
let mapDispatchToState = (dispatch) => {
  return {
    handleDelete: (data) => {
      dispatch(viewDelete(data));
    },
    handleChangeAmount: (data, option) => {
      dispatch(viewChangeAmount(data, option));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToState)(Cart);
