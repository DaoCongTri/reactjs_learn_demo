// action (object) creator
export let viewDetailAction = (shoe) => {
  return {
    type: "CHANGE",
    payload: shoe,
  };
};
export let viewAddToCart = (shoe) => {
  return {
    type: "ADDTOCART",
    payload: shoe,
  };
};
export let viewDelete = (shoe) => {
  return {
    type: "DELETE",
    payload: shoe,
  };
};
export let viewChangeAmount = (shoe, option) => {
  return {
    type: "CHANGE_QUANTITY",
    payload: {
      shoe,
      option,
    },
  };
};
