import { shoeArr } from "../../data";
// state
let initialState = {
  shoeArr,
  detailShoe: shoeArr[0],
  listCart: [],
};
export const shoeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case "CHANGE": {
      state.detailShoe = payload;
      return { ...state };
    }
    case "ADDTOCART": {
      let cloneCart = [...state.listCart];
      let index = cloneCart.findIndex((item) => item.id === payload.id);
      if (index === -1) {
        let newShoe = { ...payload, quantity: 1 };
        cloneCart.push(newShoe);
      } else {
        cloneCart[index].quantity++;
      }
      return { ...state, listCart: cloneCart };
    }
    case "DELETE": {
      let cloneCart = state.listCart.filter((item) => item.id !== payload);
      return { ...state, listCart: cloneCart };
    }
    case "CHANGE_QUANTITY": {
      let cloneCart = [...state.listCart];
      let index = cloneCart.findIndex((item) => item.id === payload.shoe);
      if (payload.option === "UP") {
        cloneCart[index].quantity++;
      } else {
        cloneCart[index].quantity--;
      }
      if (cloneCart[index].quantity === 0) {
        cloneCart.splice(index, 1);
      }
      return { ...state, listCart: cloneCart };
    }
    default:
      return state;
  }
};
