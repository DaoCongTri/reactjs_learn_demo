import React, { Component } from "react";
import ItemShoe from "./ItemShoe";
import { connect } from "react-redux";
class ListShoe extends Component {
  renderListShoe() {
    return this.props.list.map((shoe, index) => {
      return <ItemShoe key={index} data={shoe} />;
    });
  }
  render() {
    return (
      <div className="col-6">
        <h1 className="text-white bg-dark p-4 text-center">Shoes Product</h1>
        <div className="row">{this.renderListShoe()}</div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    list: state.shoeReducer.shoeArr,
  };
};
export default connect(mapStateToProps, null)(ListShoe);
