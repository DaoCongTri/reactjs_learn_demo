import React, { Component } from 'react'
import { connect } from 'react-redux'
class DemoMiniRedux extends Component {
  render() {
    console.log(this.props); // có tham số dùng arrow function
    return (
      <div className='text-center'>
        <button className='btn btn-danger' onClick={() => { this.props.handleGiam(5) }}>-</button>
        <strong className='mx-3'>{this.props.amount}</strong>
        <button className='btn btn-secondary' onClick={this.props.handleTang}>+</button>
      </div>
    )
  }
}
let mapStateToProps = (state) => { 
  return{
    amount: state.numberReducer.quantity,
  }
}
let mapDispatchToProps = (dispatch) => { 
  return {
    handleTang: () => {
      dispatch({
        type: 'TANG',
        payload: 1,
      });
    },
    handleGiam: (quantity) => { 
      dispatch({
        type: 'GIAM',
        payload: quantity,
      });
     },
  };
 };

export default connect(mapStateToProps, mapDispatchToProps)(DemoMiniRedux);