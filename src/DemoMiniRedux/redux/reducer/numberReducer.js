import { TANG } from "../constant/numberConstant";
import { GIAM } from "../constant/numberConstant";
let initialState = {
  quantity: 1,
};

export const numberReducer = (state = initialState, action) => {
  switch (action.type) {
    case TANG: {
      state.quantity++;
      return { ...state };
    }
    case GIAM: {
      state.quantity -= action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};
// mapDispatchToProps

//ko có setState => làm sao để update layout?
