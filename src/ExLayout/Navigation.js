import React, { Component } from "react";

export default class Navigation extends Component {
  render() {
    return (
      <div
        className="item-left"
        style={{
          display: "flex",
          width: "30%",
          background: "greenyellow",
          paddingBottom: 150,
        }}
      >
        <h1>Navigation Component</h1>
      </div>
    );
  }
}
