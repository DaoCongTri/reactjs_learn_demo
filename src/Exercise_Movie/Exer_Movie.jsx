import React, { Component } from 'react'
import {MovieArr} from "./data"
import ListMovie from './ListMovie';
import Detail from './Detail';
export default class Exer_Movie extends Component {
    state = {
        MovieArr,
        detail: MovieArr[0],
    };
    handleChangeDetail = (data) => { 
        // setState: bất đồng bộ
        this.setState({detail: data});
     }
  render() {
    return (
      <div>
        <h1 className='text-center bg-dark text-white p-4'>Exercise Movie</h1>
        <div className='row'>
            <ListMovie MovieArr={this.state.MovieArr} handleChangeDetail={this.handleChangeDetail}/>
            <Detail detail={this.state.detail}/>
        </div>
      </div>
    )
  }
}
