import React, { Component } from 'react'
import moment from 'moment';
export default class Detail extends Component {
  render() {
    let { maPhim, tenPhim, biDanh, trailer, moTa, maNhom, ngayKhoiChieu, danhGia,} = this.props.detail;
    return (
      <div className='col-4'>
        <div className='row'>
            <div className=''>
                <iframe src={trailer} width={500} height={300} title={tenPhim}></iframe>
                <h3>Mã Phim: {maPhim}</h3>
                <p>Tên phim: {tenPhim}</p>
                <p>Bí danh: {biDanh}</p>
                <p>Mô tả: {moTa}</p>
                <p>Mã nhóm: {maNhom}</p>
                <p>Ngày khởi chiếu: {moment(ngayKhoiChieu).format("DD-MM-YYYY")}</p>
                <p>Đánh giá: {danhGia}</p>
            </div>
        </div>
      </div>
    )
  }
}

