import moment from 'moment/moment';
import React, { Component } from 'react'
export default class ItemMovie extends Component {
    convertMovieName = (title) => { 
        if (title.length > 10) {
            return title.slice(0, 10) + "...";
        }
        return title;
     };
  render() {
    let{data, handleChangeDetail} = this.props;
    let {hinhAnh, tenPhim, ngayKhoiChieu} = data;
    return (
        <div className='col-6 pb-4'>
            <div className="card text-left" style={{height: "450px"}}>
                <img className="card-img-top" src={hinhAnh} height={300} style={{objectFit: "contain"}} alt />
                <div className="card-body">
                    <p className="card-title">{this.convertMovieName(tenPhim)}</p>
                    <p className='card-text text-secondary'>
                        {moment(ngayKhoiChieu).format("DD-MM-YYYY")}
                    </p>
                    <button className='btn btn-secondary' onClick={() => {
                        handleChangeDetail(data)
                    }}>View More</button>
                </div>
            </div>
        </div>
    )
  }
}