import React, { Component } from 'react'
import ItemMovie from './ItemMovie'
export default class ListMovie extends Component {
  render() {
    let {MovieArr} = this.props;
    return (
      <div className='col-8'>
        <div className='row'>
            {MovieArr.map((item, index) => { 
            return <ItemMovie key={index} data={item} handleChangeDetail={this.props.handleChangeDetail}/>
         })}
        </div>
      </div>
    )
  }
}
