import React, { Component } from "react";

export default class UserInfo extends Component {
  render() {
    console.log(this.props);
    return (
      <div>
        <h2>UserInfo</h2>
        <h2>UserName: {this.props.name}</h2>
        <button className="btn btn-success" onClick={this.props.handleClick}>
          Change Name
        </button>
      </div>
    );
  }
}
