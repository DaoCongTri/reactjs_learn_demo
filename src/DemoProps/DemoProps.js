import React, { Component } from "react";
import UserInfo from "./UserInfo";
export default class DemoProps extends Component {
  state = {
    username: "Rosé",
  };
  handleChangeName = () => {
    // toogle
    this.setState({
      username: this.state.username == "Rosé" ? "Park Chaeyoung" : "Rosé",
    });
  };
  render() {
    return (
      <div>
        <h2>DemoProps</h2>
        <UserInfo
          name={this.state.username}
          handleClick={this.handleChangeName}
        />
      </div>
    );
  }
}
