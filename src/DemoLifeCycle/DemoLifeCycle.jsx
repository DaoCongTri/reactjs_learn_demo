import React, { Component } from "react";
import axios from "axios";
// import Child from './Child';
export default class DemoLifeCycle extends Component {
  // constructor sẽ chạy trước tất cả mọi thứ và dùng để khai báo state và những giá trị khác
  constructor(props) {
    super(props);
    this.state = {
      message: "Hello",
      number: 1,
      isLike: false,
      isShow: true,
      listProduct: [],
    };
    console.log("constructor");
  }
  /*
    //---------------------------------------------
    // Trước version 16.4
    // Method componentWillUnmount: chạy trước render, nó bị thay thế bởi getDerivedStateFromProps từ 16.4 trở đi
    // componentWillUnmount(){
    //     console.log("componentWillUnmount");
    // }
    //----------------------------------------
    // method getDerivedStateFromProps sẽ chạy trước render, mặc định có return: null hoặc là {key: value}
    // props: nhận prop từ component
    // state: nhận state từ component
    static getDerivedStateFromProps(props, state){
        console.log("getDerivedStateFromProps", props, state);
        return {
            ...state,
            message: "Park Chaeyoung",
        };
    }
    // method shouldComponentUpdate: chạy sau getDerivedStateFromProps và trước render() và mặc định sẽ return về true: true ? render và không render
    shouldComponentUpdate(){
        return true;
    }
    // method getSnapshotBeforeUpdate: chạy sau khi render và trước componentDidUpdate, lưu ý nó sẽ báo lỗi khi không khai báo componentDidUpdate
    getSnapshotBeforeUpdate(){
        console.log("getSnapshotBeforeUpdate");
        return {
            message: "snapshot value",
        }
    }
    // chạy trong giai đoạn updating, và không nên setState trong này - Dùng để  call api phụ thuộc url
    componentDidUpdate(nextProps, nextState, snapshot){
        let getParamUrl = window.location.search;
        let searchParam = new URLSearchParams(getParamUrl);
        let id = searchParam.get("id");
        if (id) {
            // call api
        }
        console.log("componentDidUpdate");
    }*/
  render() {
    console.log("render", this.state.listProduct);
    const { listProduct } = this.state || {};
    return (
      <div className="container">
        DemoLifeCycle
        {/* <p>{message}</p>
            <p>{number}</p>
            <button className='btn btn-success' onClick={() => this.setState({number: number + 1})}>Increase Number</button>
            <div id='content'></div>
            <button className='btn btn-secondary' onClick={() => this.setState({isLike: !isLike})}>Like</button>
            <button onClick={() => 
                this.setState({isShow: !isShow})
            }>Show/Hidden Child</button>
            {isShow ? <Child number={number}/> : null } */}
        <h3>List Product</h3>
        <div className="row">
          {listProduct.map((item) => {
            return (
              <div className="col-4 mb-3" key={item.id}>
                <div className="card">
                  <img src={item.image} alt="123" />
                  <div className="card-body">
                    <div className="card-title">Product Name</div>
                    <div>
                      <button className="btn btn-primary">View Detail</button>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
  getProductListApi() {
    axios
      .get("https://shop.cyberlearn.vn/api/Product")
      .then((res) => {
        console.log(res);
        this.setState({ listProduct: res.data.content });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  componentDidMount() {
    // call api => data => setState
    // document.getElementById("content").innerHTML = 1234567;
    this.getProductListApi();
    console.log("componentDidMount");
  }
}
const { 
    a = 'default', 
    b, 
    ...rest 
} = {
    b: 'val1',
    c: 'val2',
    d: 'val3'
};

console.log(a, b, rest); 