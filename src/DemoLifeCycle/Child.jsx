import React, { Component, PureComponent } from 'react'
// PureComponent: nó sẽ tích hợp sẵn
export default class Child extends PureComponent {
    constructor(props){
        super(props);
        this.state = {
            numberChild: 1,
        };
        console.log("constructor child");
    }
    static getDerivedStateFromProps(props, state){
        console.log("getDerivedStateFromProps");
        return {
            ...state,
            numberChild: props.number*2,
        };
    }
    shouldComponentUpdate(nextProps, nextState){
        console.log("shouldComponentUpdate", nextProps, nextState, this.props);
        return true;
    }
  render() {
    console.log("render child");
    return (
      <div>
        Child: {this.state.numberChild}
      </div>
    )
  }
  componentDidMount(){
    // call api => data => setState
    console.log("componentDidMount child");
  }
}
