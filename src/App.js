// import logo from "./logo.svg";
import "./App.css";
import ManagentProduct from "./ReactForm/ManagentProduct";
/*
import Header from "./DemoComponent/Header";
import Banner from "./DemoComponent/Banner";
import NavTabs from "./DemoComponent/NavTabs";
import Carts from "./DemoComponent/Carts";
import Header from "./ExLayout/Header";
import Navigation from "./ExLayout/Navigation";
import Content from "./ExLayout/Content";
import Footer from "./ExLayout/Footer";
import DataBiding from "./DataBinding/DataBiding";
import EventHandling from "./EventHandling/EventHandling";
import DemoState from "./DemoState/DemoState";
import ExStateCar from "./Ex_State_Car/ExStateCar";
import RenderWithMap from "./RenderWithMap/RenderWithMap";
import DemoProps from "./DemoProps/DemoProps";
import ExShoeShop from "./ExerciseShoeShop/ExShoeShop";
import Exer_Movie from "./Exercise_Movie/Exer_Movie";
import DemoMiniRedux from "./DemoMiniRedux/DemoMiniRedux";
import ExShoeShopRedux from "./ExShoeShopRedux/ExShoeShopRedux";
import DemoLifeCycle from "./DemoLifeCycle/DemoLifeCycle";
*/

function App() {
  return (
    <div>
      {/* <Header />
      <Banner />
      <NavTabs /> */}
      {/* <Carts /> */}
      {/* <Header />
      <div style={{ display: "flex" }}>
        <Navigation />
        <Content />
      </div>
      <Footer />
      <div className="container">
        <DataBiding />
        <EventHandling />
        <DemoState />
        <ExStateCar />
      </div> */}
      {/* <RenderWithMap /> */}
      {/* <DemoProps /> */}
      {/* <ExShoeShop /> */}
      {/* <Exer_Movie /> */}
      {/* <DemoMiniRedux /> */}
      {/* <ExShoeShopRedux /> */}
      {/* <DemoLifeCycle /> */}
      <ManagentProduct />
    </div>
  );
}

export default App;
